import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { useRouter, useRoute } from "vue-router";

export const useAuthStore = defineStore("auth", () => {
    const router = useRouter();
    const route = useRoute();

    const loginModalOpen = ref(false);
    const loginMsg = ref("");
    const loginToken = ref(localStorage.getItem("token"));
    const isLogged = ref(!!loginToken.value);
    const toNavigate = ref("/");

    function loginAction(username: string, pass: string): boolean {
        console.log(route.fullPath);
        if (username === "admin" && pass === "admin") {
            localStorage.setItem("token", "THISISLOGINTOKEN");
            isLogged.value = true;
            loginMsg.value = "";
            router.push({ path: toNavigate.value });
            toNavigate.value = "/"
            return true;
        } else {
            return false;
        }
    }

    function logoutAction(): void {
        localStorage.removeItem("token");
        isLogged.value = false;
        router.push({ path: "/" });
    }

    return {
        loginModalOpen,
        loginMsg,
        isLogged,
        loginToken,
        toNavigate,
        loginAction,
        logoutAction,
    };
});
