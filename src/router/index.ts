import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    }, 
    {
      path: '/products',
      name: 'products',
      component: () => import ('../views/ProductsView.vue'),
      meta: { requiresAuth: true },
    },
    {
      path: '/photos',
      name: 'photos',
      component: () => import('../views/PhotosViews.vue'),
    },
    {
      path: '/photos/:photoId',
      name: 'singlePhoto',
      component: () => import('../views/SinglePhoto.vue')
    },
    {
      path: '/about', 
      name: 'about', 
      component: () => import('../views/About.vue')
    },
  ]
})

import { useAuthStore } from '@/stores/auth'

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const hasToken = localStorage.getItem('token')

  if (requiresAuth && !hasToken) {
    const auth = useAuthStore()
    auth.loginModalOpen = true;
    auth.loginMsg = 'You need to login first';
    auth.toNavigate = to.fullPath;
    next(false)
  } else {
    next() 
  }
})

export default router
